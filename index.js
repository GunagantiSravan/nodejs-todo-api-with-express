const express = require('express')
const app = express();
const port = process.env.PORT || 3000 ;

app.use(express.json());
const { json } = require('body-parser');
const todosData=require('./todos')

app.get("/todos/", async (request, response) => {
    try {
        if(todosData.length==0){
            response.status(400).json({message: "Todo items are not found"})
        }else{
            response.status(200).json(todosData);    
        }
        
    } catch (error) {
        response.status(400).json({ERROR: `${error.message}`})
    }
    
  });


app.get("/todos/:id", async (request, response) => {

    const todoId = request.params.id;
    
    const todo = todosData.find(todo=>todo.id === parseInt(todoId))
    try {
        if (todo) {
            todosData.push(todo)
            response.status(200).json(todo);
        } else {
            response.status(400).json({msg:`todo with id ${todoId} not found`})
        } 
        
    } catch (error) {
        response.status(400).json({ERROR: `${error.message}`})
    }
            
    
  });

app.post("/todos/", async (request, response) => {
    const todo=request.body;
    try {
        if ((todo.text) && (todo.created_at) && (todo.Tags) && (todo.is_complete)) {
            let newTodo={
                id:todosData.length+1,
                text:todo.text,
                created_at:todo.created_at,
                Tags:todo.Tags,
                is_complete:todo.is_complete

            }
            todosData.push(newTodo)
            response.status(200).json({message : "Todo is created successfully", newTodo})
        }else {
            response.status(400).json({message:"Please all the fields."})
        }

    }catch (error) {
        response.status(400).json({ERROR: `${error.message}`})
        
    }
  });

  app.put("/todos/:id", async (request, response) => {

    const todoId = request.params.id;
    const todo=request.body;
    
    const todo_ = todosData.find(todo=>todo.id === parseInt(todoId))
    const todoIndex=todosData.findIndex(todo=>todo.id === parseInt(todoId))
    try {
        if (todo_) {
            if ((todo.text) && (todo.created_at) && (todo.Tags) && (todo.is_complete)) {
                let updatedTodo={
                    id:parseInt(todoId),
                    text:todo.text,
                    created_at:todo.created_at,
                    Tags:todo.Tags,
                    is_complete:todo.is_complete
    
                }
                todosData.splice(todoIndex,1,updatedTodo)
                response.status(200).json({message : "Todo updated successfully", updatedTodo})
            }else {
                response.status(400).json({message:"Please all the fields."})
            }

        } else {
            response.status(400).json({msg:`todo with id ${todoId} not found`})
        } 
    }catch (error) {
        response.status(400).json({ERROR: `${error.message}`})
        
    }

  });

  app.patch("/todos/:id", async (request, response) => {

    const todoId = request.params.id;
    const todo=request.body;
    
    const foundTodo = todosData.find(todo=>todo.id === parseInt(todoId))
    console.log(foundTodo);
    const todoIndex=todosData.findIndex(todo=>todo.id === parseInt(todoId))
    try {
        if (foundTodo) {
            let updated_todo={
                id:parseInt(todoId),
                text:(todo.text)?todo.text:foundTodo.text,
                created_at:(todo.created_at)?todo.created_at:foundTodo.created_at,
                Tags:(todo.Tags)?todo.Tags:foundTodo.Tags,
                is_complete:(todo.is_complete)?todo.is_complete:foundTodo.is_complete

            }
            todosData.splice(todoIndex,1,updated_todo)
            response.status(200).json({message : `Todo updated succefully`})
        } else {
            response.status(400).json({msg:`todo with id ${todoId} not found`})

        }
    } catch (error) {
        response.status(400).json({ERROR: `${error.message}`})
    }
    
  });

  app.delete("/todos/:id", async (request, response) => {

    const todoId = request.params.id;
    
    const todo = todosData.find(todo=>todo.id === parseInt(todoId))
    const todoIndex=todosData.findIndex(todo=>todo.id === parseInt(todoId))
    console.log(todo,todoIndex);
    try {
        if (todo) {
                todosData.splice(todoIndex,1)
                response.status(200).json({msg:`Todo with id ${todoId} deleted successfully`})
            } else {
                response.status(400).json({msg:`Todo with id ${todoId} not found`});

            }
    } catch (error) {
        response.status(400).json({ERROR: `${error.message}`})   
    }
            
  });

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
})